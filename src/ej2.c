#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

void manejador(int senal);
int main(int argc, char *argv[]){
  if(argc!=2){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
    }
  int senal = atoi(argv[1]);
  while(1){
    signal(senal, manejador);
  }
}
void manejador(int senal){
  printf("Señal recibida= %d y terminando.\n", senal);
  _exit(senal);
}
