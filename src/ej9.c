#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#define BUFFER_SIZE 1024

int main(int argc, char *argv[]){ 
    if(argc<2){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
  }
  int tuberia, n, estado;
  char buffer[BUFFER_SIZE];
  if((tuberia = open(argv[1], O_RDONLY)) ==-1){
    return EXIT_FAILURE;
  }  
  while(1){
    n=read(tuberia, buffer, BUFFER_SIZE);
    if((estado=(write(1, buffer, n)))==-1){
      fprintf(stderr, "Error de escritura");
      return EXIT_FAILURE;
    }
    else if(estado<BUFFER_SIZE){
      sleep(1);
    }
  }
  return EXIT_SUCCESS;
}
