#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
void manejador(int senal);
int main(int argc, char *argv[]){
  if(argc!=2){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
  }
  if(argv[1]<0){
    fprintf(stderr, "Los segundos no pueden ser negativos");
    return EXIT_FAILURE;
  }
  
  int segundos = atoi(argv[1]);
  int nAlrm;
  while(1){
    alarm(segundos);
    signal(SIGALRM,manejador);
    pause();
    nAlrm++;
    printf("Alarma %d\n", nAlrm);
  }
}
void manejador(int senal){
}