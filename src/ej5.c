#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
int main(int argc, char *argv[]){
  if(argc!=3){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
  }
  int senal = atoi(argv[1]);
  pid_t pidProceso = atoi(argv[2]);
  if(kill(pidProceso, senal)==-1)
    printf("Error en el uso de kill");
  return EXIT_SUCCESS;
}