#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
int main(int argc, char *argv[]){
  
  if(argc!=2){
    fprintf(stderr, "Línea de órdenes incorrecta\n");
    return EXIT_FAILURE;
  }
  while(1){
    int senal = atoi(argv[1]);
    signal(senal, SIG_IGN);
  } 
}
