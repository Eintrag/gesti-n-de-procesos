#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
int main(int argc, char *argv[]){
  if(argc<3){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
  }
  int nArg = argc-2; 
  char *pArg[nArg];
  int i, archivo;
  for(i = 0; i<nArg; i++){
    pArg[i] = argv[i+2];
  }
  archivo = open(argv[1], O_WRONLY|O_CREAT|O_TRUNC, 0644);
  if(close(1)==-1){
    fprintf(stderr, "Error al cerrar el descriptor");
    return EXIT_FAILURE;
  }
  if(dup(archivo)==-1){
    fprintf(stderr, "Error al establecer el nuevo descriptor");
    return EXIT_FAILURE;
  }
  if(execvp(argv[2], pArg)==-1){
    fprintf(stderr, "Error al ejecutar el programa en el archivo");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}