#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
void subp(int fd[2], char *prog, int modo);
int main(int argc, char *argv[]){
  int pid, i, fd[2], estado;
  if(argc!=3){
    fprintf(stderr, "Línea de órdenes incorrecta");
    return EXIT_FAILURE;
  }
  if (pipe(fd)==-1){
    fprintf(stderr, "Error en la creación de la tubería");
    return EXIT_FAILURE;
  }
  for(i=0; i<2; i++){
    if((pid=fork())==-1){
      fprintf(stderr, "Error en la creación del hijo %d", i+1);
    }
    if(pid==0){
      switch(i){
	case 0:
	  subp(fd,argv[1],0);
	case 1:
	  subp(fd,argv[2],1);
      }
    }
  }
  for(i=0; i<2; i++){
    if(close(fd[i])==-1){
      fprintf(stderr, "Error al cerrar");
      return EXIT_FAILURE;
    }
    wait(&estado);
  }
  return EXIT_SUCCESS;
}
void subp(int fd[2], char *prog, int modo){
  close(fd[0+modo]);
  if(modo==0){
    if(dup2(fd[1], STDOUT_FILENO)==-1){
      fprintf(stderr, "Error de salida");
      exit(EXIT_FAILURE);
    }
  }
  else if((dup2(fd[0], STDIN_FILENO))==-1){
    fprintf(stderr, "Error de entrada");
    exit(EXIT_FAILURE);
  }
  if(execlp(prog, prog, NULL)==-1){
    fprintf(stderr, "Error en los hijos");
    exit(EXIT_FAILURE);
  }
}