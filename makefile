DIROBJ := obj/
DIREXE := exec/
DIRSRC := src/

CFLAGS := -c -Wall
LDLIBS := -lrt
CC := gcc

all : dirs escudo1 escudo2 escudo3 alarma mikill entrada salida tuberia consumidor

dirs:
	mkdir -p $(DIROBJ) $(DIREXE)
escudo1: $(DIROBJ)ej1.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
escudo2: $(DIROBJ)ej2.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
escudo3: $(DIROBJ)ej3.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
alarma: $(DIROBJ)ej4.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)

mikill: $(DIROBJ)ej5.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
entrada: $(DIROBJ)ej6.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)

salida: $(DIROBJ)ej7.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
tuberia: $(DIROBJ)ej8.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)
	
consumidor: $(DIROBJ)ej9.o
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS)

$(DIROBJ)%.o: $(DIRSRC)%.c
	$(CC) $(CFLAGS) $^ -o $@

clean: 
	rm -rf *~ core $(DIROBJ) $(DIREXE) $(DIRSRC)*~

